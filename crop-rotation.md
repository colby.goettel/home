# Crop rotation
## Overview
This document is meant to show the placement of various crops over the past several years.

## Placement
The northernmost bed is 1, then 2 and 3 to the south. The 8x8 and 4x8 are the attached beds on the south side of the yard. The list starts at the east end of the bed and works west. If that doesn't make sense, _you_ write the doc next time.

### 2018
#### Raised bed 1
- Heirloom tomato Arkansas traveler x2, heirloom tomato green zebra
- Heirloom tomato box car Willie, yellow crookneck squash
- Yellow onion x6
- Yellow crookneck squash
- Yellow straight neck squash x3
- Yellow straight neck squash x3

#### Raised bed 2
- Chef Jeff's tomato salsa Roma x2, Chef Jeff's tomato lemon boy
- Cauliflower x3
- Cauliflower x3
- Yellow onion x6
- Chef Jeff's pepper mucho nacho, Chef Jeff's pepper habanero x2
- California wonder sweet pepper x3
- California wonder sweet pepper x3

#### 8'x8' raised bed
- Kieffer pear tree in middle
- NE and SE: heirloom pole bean Kentucky wonder
- NW and SW: 2x yellow crookneck squash each

#### Attached 4'x8'
- Chef Jeff's bush bean blue lake x2
- Serrano x3
- Serrano x3

### 2019
#### Raised bed 1
- Jubilee yellow heirloom tomato, sweet basil, San Marzano tomato
- Lemon basil, strawberry, lemon basil
- Jubilee, San Marzano
- Anaheim pepper
- Anaheim, habanero
- Anaheim, habanero

#### Raised bed 2
- San Marzano, San Marzano
- Sweet basil, strawberry, terragon
- San Marzano, San Marzano
- Strawberry
- Pepper mucho nacho, mucho nacho
- Habanero, mucho nacho

#### Attached 4'x8'
- Zucchini zig zag:
```
-----
|x  |
|  x|
|x  |
-----
```

### 2020
#### Raised bed 1
- Sport pepper, Scotch bonnet
- 2x jalapeno
- 2x golden boy

#### Raised bed 2
- 2x Anaheim
- 2x habanero
- 2x golden boy

#### Attached 4x8
- 6x blue lake bush bean
