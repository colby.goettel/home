# Thought on what's been grown
## Beans
- The insects ate the heck out of all the bean leaves. Research how to stop that.
- **Chef Jeff's bush bean blue lake**: these didn't grow all that well. They're good beans, but I really forsook that whole raised bed and they really suffered because of it.
    - Verdict: I don't really eat green beans. What's the point?
- **Heirloom pole bean Kentucky wonder**: grew some fruit. Was okay raw. Haven't had enough to harvest to bring in and cook. No real opinion on the fruit. The stalk grows like crazy. I had three and a half foot trellises and they were overrun before any fruit production happened. Go for five or six foot trellises with an arch (don't do that on the 8x8 bed).
    - Verdict: Probably wouldn't do these again.

## Peppers
- **California wonder sweet pepper**: these are just green bell peppers. Whatever. They're decent filler, but I'd rather have Anaheims for that. And green/red peppers suck.
    - Verdict: hard pass.
- **Cauliflower**: this didn't grow at all (planted six, only two "made it"). And I don't even really like it, so just buy it if you want it. Not worth the hassle and space.
- **Habanero**: I haven't done anything to these plants besides water and they've done amazingly well. The fruit isn't ripe yet, but there's going to be a massive harvest.
    - Harvest: holy cow. Each plant produces like a hundred peppers. I've got enough bottles now so I think 3 plants is perfect.
    - Verdict: grow three plants. Harvest a lot of the green peppers.
- **Mucho nacho (Chef Jeff)**: Love these. They're a hybrid jalapeno. They're especially good when they vine ripen to red (get that sweet heat going on).
    - Verdict: Definitely grow multiples of these. Three should be sufficient.
- **Serrano**: I wouldn't grow serranos again. I don't even like them that much, and six plants' worth was an insane amount to have.

## Onions
- **Yellow onions**: They were incredible. Gotta start plucking the green onions earlier so we get larger onions, but honestly, the two inch bulbs were incredible and I loved them. The greens were great.
    - Verdict: twelve onion bunches has been perfect.

## Squash (summer)
- **Yellow crookneck squash**: these never really grew. Wouldn't do again.
- **Yellow straight neck squash**: the yellow squash plants were great and produced a bunch of squash. However, there were six plants in an area that should have been supporting two or three plants. They get absolutely massive and need room so they don't get disease. Additionally, they were blown apart by wind storms, so look into some sort of covering. The current tarps are too much hassle and you're never going to do that again.
    - Verdict: plant them way more spaced, but definitely plant squash

## Tomatoes
- [x] Set up a much larger trellis. 4x4 posts and cattle fence should work great.
- The 4' width is only good for two tomato plants. Have the trellis on each side and have the tomatoes go up the trellis. Make sure you prune the tomatoes or it's not going to work.

- **Arkansas Traveler**: too big. Not a fan. Wouldn't do again.
- **Box Car Willie**: Didn't really grow. And not the kind of tomato I like anyway.
- **Lemon Boy**: these were really good. Like, insanely good.
    - Verdict: two or three bushes.
- **Roma**: I really struggled getting healthy roma tomatoes. It might have been a crowding issue.
