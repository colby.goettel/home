# Garden and tree layout plan
The yard has many plants in it. This document is designed to explain what everything is, but is also for planning so there are two layouts: existing and planned.

There are many plants that were already present when I moved in which I don't intend to identify (most are, though). However, new plants that I put in will be documented.

## Layouts
### Legend
| Symbol | Meaning                 |
| ------ | ----------------------- |
| 1-2    | Garden beds, see below  |
| 4x8    | 4x8 bed, see below      |
| a      | Azalea                  |
| B      | Bamboo                  |
| b      | Boxwood                 |
| c      | Compost heap            |
| d      | White Flowering Dogwood |
| E      | Spruce Evergreen        |
| e      | Electrical utility box  |
| F      | Fire pit                |
| f      | Faucet                  |
| H      | Holly                   |
| h      | Hydrangea               |
| j      | Japanese maple          |
| K      | Kieffer pear tree       |
| k      | Knockout roses          |
| M      | Moonglow pear tree      |
| N      | Navaho blackberry       |
| n      | Nandina                 |
| Pe     | Elberta peach           |
| Pb     | Belle of Georgia peach  |
| p      | Sargent Crabapple       |
| R      | Rose of Sharon          |
| r      | American Redbud         |
| S      | Saskatoon berry trees   |
| s      | Shade tree(s?)          |
| T      | Old growth tree*        |
| Tu     | Tuliptree               |
| t      | Transformers            |
| u      | Unknown                 |
| W      | Wood pile               |
| w      | Washington Hawthorn     |
| x      | Garden beds, see below  |
| y      | Japanese yew shrub      |

*&ast; Here when I moved in.*

### Existing layout
```
<-N
|-------Back fence---------------------------------|
|            w C   NN  11  22       p r d      T   |
|            w C   NN  11  22       r r d    Pb  e |
|                  NN  11  22                      |
|                  xx  xx  xx                |-----|
|W                                           |  x  |
|W     F                                     |     |
|                                            |-----|
|                   n  R  n   /\aa             |4x8|
|              y    n     n  /  \ M            |4x8|
|           j y E|---Deck----\  /--| k         |4x8|
|           yyaaH|            \/   | R k k     |---|
|          |--House-----------\/-----------|       |
|          |                            f  |t      |
|         h|                               |t      |
|         h|                               |t      |
|         h|f                              |       |
|         h|                               |       |
|         h|                               |       |
|          |                               |       |
|          |                               |       |
|Gate|-----|                               |       |
     |                                     |       |
     |                      __             |-------|
     |                     | u|            |
     |                     | j|            |
     |                     |  |__f_________|
     |_____________________|      bbb    R
       |                |
       |                |
       |                |
```

## Plans
- [x] Raised garden beds
  - [x] Two southern beds for vegetables
  - [x] Northern bed for blackberries
- [ ] Saskatoon berry bushes.
- [ ] Strawberries would be nice.
