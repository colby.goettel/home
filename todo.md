# TODO
## Overview
This document lists all of the todo work in and around the house.

### Notation
For project-type work, symbols will precede the work: W stands for weekend (as in a job that can be done in a weekend) and $ stands for cost. Each of these can appear up to three times, or four times to denote serious work (e.g., W$, WW$, W$$$). $ is in the <$50 range, $$ in the $50-100 range, and $$$ is $100+.

## Move ready
- [] Master bathroom shower
- [] Master bathroom toilet seat replaced
- [] Drywall work in entry hall
    - [] Paint entry hall
- [] Island conduit
- [] Deck fan removed
- [] Clean up yard
- [] Fix side gate
- [] Toilet plunger replaced
- [] Oven heating element
- [] Paint living room
- [] Paint pink room
- [] Laundry room moulding and paint

## Inside
### Attic
- [ ] W$ Add flooring around furnace

### Garage
- [ ] W$$ LED overhead lights (buy one for laundry room, too)
- [x] Drill out deadbolt hole

### Neapolitan room
- [ ] WW$$ Paint

### Hallways
- [ ] W$$ Paint

### Kitchen
- [ ] W$ Island conduit
- [ ] W$ Fix drawers
- [ ] WW?$ Convert cabinet into garbage and recycling drawer

### Laundry room
- [ ] W$ Cabinet molding and associated painting
- [ ] W$$ LED lighting

### Living room
- [ ] W$$ LED can lights
- [ ] Figure out what to do with built-in media console
- [ ] W$$ Paint room

### Master bathroom
- [ ] W$ Replace toilet seat
- Shower
  - [ ] Replace shower with tile
- [ ] Exhaust fan (replace light with it?)

### Master bedroom
- [ ] Second night stand

## Outside
### Front yard
- [ ] Replace side gate latch
- [ ] Fix concrete on driveway

### Back yard
- [ ] What are we doing with the deck?
  - [ ] Stain
  - [ ] Pergola
    - [ ] Vertical poles aren't big enough. Do they need to be 6x6?
    - [ ] Top is warped
    - [ ] Remove fan or run conduit for proper power
- [ ] Bury furnace overflow pipe on N in French drain
  - Call before you dig
- [ ] Replace fence post N and E
